
<!-- .slide: data-state="titleslide" -->

# Solinea Brownbag

## Baking Images - Packer & Openstack

<span class="month-year">June 2015</span>


---

## Agenda
- Kick off Demo
- Image Building Sucks!
- What is Packer?
- Intro to Templates
- Template Building Blocks
 - Builders
 - Provisioners
 - Post-processors
- Walkthrough and Demo

---

## Kick Off Demo
![Standby][standby] <!-- .element: style="border: 0; width:60%; box-shadow: none;" -->

[standby]: /assets/standby.jpg

---

## Image Building Sucks!
###Here's why:

- Collection of bash scripts
 - Hope they ran in the right order

- Base images can be different
 - Which image did I use last time? 
 - Did its name change?

- Tough to version control
 - CM scripts living on a dev box?
 - Are base images spinning up the same way every time?

- No logs to take home as a souvenir :(

<!-- .element: style="max-width: 55%; float:left; font-size: 0.6em;" -->

![Pup][pup] <!-- .element: style="float: right; border: 0; width:40%; box-shadow: none;" -->

[pup]: /assets/dog-computer.jpg

---

## What is Packer ?

![Packer Logo][packer]
[packer]: /assets/packer-logo.png
<!-- .element: style="float: left; border: 0; box-shadow: none;" -->

- Developed by Hashicorp (of Vagrant fame)

- Automates the image creation process

- Supports a variety of clouds

- Templates are easy to write and version

- Create several images simultaneously

<!-- .element: style="max-width: 60%; float:right; font-size: 0.8em;" -->

---

## Intro to Templates
- Written in JSON

- 3 Building Blocks:

 - Builders

 - Provisioners

 - Post-Processors

<!-- .element: style="max-width: 50%; float:left; font-size: 0.7em;" -->

```json
{
  "builders": [
    {
     ...
    }
  ],
  "provisioners": [
    {
     ...
    }
  ],
  "post-processors": [
    {
     ...
    }
  ]
}
```
<!-- .element: style="font-size: 0.4em; max-width: 50%; float: right" -->


---

## Template Building Blocks
### Builders

- Array of different clouds to build against

- Can build on multiple clouds in parallel
 - Different colors in terminal output for each environment

- Supports lots of different builders:
 - Docker
 - OpenStack
 - Amazon
 - GCE
 - VMWare
 - etc.

<!-- .element: style="max-width: 40%; float:left; font-size: 0.6em;" -->

```json
  "builders": [
    {
     "type": "openstack",
     "provider": "https://tcloud.solinea.com:5000/v2.0/",
     "ssh_username": "ubuntu",
     "project": "solinea",
     "region": "RegionOne",
     "image_name": "Spencer's Test Image",
     "source_image": "0d463322-1d11-44dc-9270-bb1021d9f651",
     "flavor": "4",
     "networks": ["871b996f-8154-4d38-8b3c-fbb65e2348e1"],
     "use_floating_ip": true
    },
    {
     "type": "amazon-ebs",
     ...	
    }
  ],
```
<!-- .element: style="font-size: 0.4em; max-width: 55%; float: right" -->

---

## Template Building Blocks
### Provisioners

- The "do stuff" portion of the bake

- Packer logs into image via SSH to provision

- Provisioners run in serial, one after the other

- Can chain them together to do cool stuff

- Again, lots of options:
 - File Uploads
 - Shell Scripts
 - Ansible
 - Chef
 - Puppet
 - Salt

<!-- .element: style="max-width: 40%; float:left; font-size: 0.5em;" -->

```json
   "provisioners": [
    {
      "type": "file",
      "source": "../files/myfile.tar.gz",
      "destination": "/home/ubuntu/myfile.tar.gz"
    },   
    {
      "type": "shell",
      "inline": [
        "sudo apt-get update",
        "sudo apt-get install -y ansible"
      ]
    }
   ]
```
<!-- .element: style="font-size: 0.4em; max-width: 60%; float: right" -->

---

## Template Building Blocks
### Post-processors

- Packages your images up with a nice little bow

- Unnecessary for OpenStack or Amazon

- Does things like:
 - Register image in vSphere
 - Auto-create Vagrantfiles
 - Push to Docker registry
 - Compress image

<!-- .element: style="max-width: 50%; float:left; font-size: 0.5em;" -->

```json
  "post-processors": [
    {
      "type": "compress",
      "format": "tar.gz"
    }
  ]
```
<!-- .element: style="font-size: 0.4em; max-width: 50%; float: right" -->

---

## Walkthrough and Demo
#### packer-ansible-ubuntu-wordpress.json

```json
{
  "builders": [
    {
      "type": "openstack",
      "provider": "https://tcloud.solinea.com:5000/v2.0/",
      "ssh_username": "ubuntu",
      "project": "solinea",
      "region": "RegionOne",
      "image_name": "Ubuntu 14.04 - Wordpress",
      "source_image": "0d463322-1d11-44dc-9270-bb1021d9f651",
      "flavor": "4",
      "networks": ["871b996f-8154-4d38-8b3c-fbb65e2348e1"],
      "use_floating_ip": true
    }
  ],
   "provisioners": [
   {
     "type": "file",
     "source": "../files/wordpress.tar.gz",
     "destination": "/home/ubuntu/wordpress.tar.gz"
   },   
   {
     "type": "file",
     "source": "../files/wp-config.php",
     "destination": "/home/ubuntu/wp-config.php"
   },
   {
    "type": "shell",
    "inline": [
      "sleep 10",
      "sudo apt-get update",
      "sudo apt-get install -y ansible"
    ]
   },
   {
    "type": "ansible-local",
    "playbook_file": "../provisioners/provision-wordpress.yml"
   }
  ]
}
```

<!-- .element: style="font-size: 0.4em;" -->

---

## Walkthrough and Demo
#### packer-wordpress.yml

```yaml
---
- hosts: localhost
  remote_user: ubuntu
  sudo: yes

  tasks:
  - name: Install PHP and MySQL Prereqs
    apt: name={{ item }} state=present
    with_items:
    - mysql-server
    - php5
    - php5-mysql
    - python-mysqldb

  - name: Create MySQL DB
    mysql_db: name=wordpress state=present

  - name: Setup a Wordpress Database User
    mysql_user: name=wordpress password=testpass priv=wordpress.*:ALL state=present

  - name: Unzip Wordpress to /var/www/html
    unarchive: src=/home/ubuntu/wordpress.tar.gz dest=/var/www/html copy=no

  - name: Move wp-config-sample to wp-config
    command: mv /home/ubuntu/wp-config.php /var/www/html/wordpress/wp-config.php

  - name: Wait for 10 secs to avoid Packer Race Condition
    command: sleep 10
```

<!-- .element: style="font-size: 0.4em;" -->

---

<!-- .slide: data-state="sectionslide" class="sectionslide" -->

#### Baking Images - Packer & Openstack
## Questions?

<p style="z-index: -1; opacity: 0.3; position: absolute; top: -60px; left: -100px; overflow: hidden; width: 11in; height: 8in;">
  <img data-src="/assets/solinea-emblem-big.png" class="printonly" style="border: none; box-shadow: none; background: none; width: 11in; height: 11in; max-width: 200%; max-height: 200%"/>
</p>
