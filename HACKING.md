
## Course Numbering

Courses are prefixed with a topic designator and then numbered by difficulty, specialization and duration. An example of this would be OS101 or DK102.

Topic designator refers to the course subject:

* OSxxx is for OpenStack training
* DKxxx is for Docker training
* DOxxx is for DevOps training

Difficulty follows the normal college numbering sequences:

* 100s are for introductory course that expect no prior knowledge
* 200s build upon introductory knowledge to provide practitioner experience
* 300s are advanced classes

The topic number refers to a topic of a class. These are just serial numbers like OS314 where the 1 refers to network technology version of that class.

Duration is in days with zero designated for less that one day:

* 0 is less than one day
* 1 for one day
* 2 for two days
* 3 for three days
* 4 for four days
* 5 for five days

Examples of this numbering are:

* OS101 is a one day introductory course
* OS204 is a four day operations class
* OS314 is a four day networking class based on Cisco technologies
* OS320 is a 1/2 day performance class

## Creating PDFs

The version of phantomjs that comes with the npm install doesn't properly handle the fonts we use. To upgrade I changed the exports.version from 1.9.8 to 2.0.0 in:

- node_modules/grunt-contrib-qunit/node_modules/grunt-lib-phantomjs/node_modules/phantomjs/lib/phantomjs.js

However, there appears to be a problem with the osx binary for 2.0.0. To fix, grab a copy of upx and decompress the phantomjs binary:

- `upx -d node_modules/grunt-contrib-qunit/node_modules/grunt-lib-phantomjs/node_modules/phantomjs/lib/phantom/bin/phantomjs`

Now we can automate the creation of PDFs using the following command. Presumably grunt has some mechanism for making this easy but I haven't read enough about grunt to figure that out yet.

```
node_modules/grunt-contrib-qunit/node_modules/grunt-lib-phantomjs/node_modules/phantomjs/bin/phantomjs \
plugin/print-pdf/print-pdf.js http://localhost:8000/mod-1-intro.html?print-pdf ~/Desktop/foo.pdf 528x385
```
